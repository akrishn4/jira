**Spin up infrastructure on AWS using cloud formation template to automatically install and run jira**

Here you have a cloud formation template (*jira-stack.yml*) which spins up following infrastructure
```
* t3.large ec2 instance of type Amazon Linux 2 AMI (HVM), SSD Volume Type - ami-0e38b48473ea57778 (64-bit x86) / ami-0fb3bb3e1ae2da0be (64-bit Arm) 
* t2.micro RDS instance
* Creates relevant Security Group to enable running of jira software
* Installs ansible, git, fontconfig and firewalld packages
* Downloads/clones ansible package from git
* Executes ansible playbook which deploys jira software.
```

*Note: This is tested on Amazon Linux 2 AMI - ami-0fb3bb3e1ae2da0be. This should work on any RHEL /CentOs distribution of version 7 and above where systemd is present.*

We have ansible playbook (jira-install.yml) which downloads and installs jira installer and sets up jira service script and opens up port in firewalld for jira to run on port 8080

How to run -

1. Generate aws pem/ppk file under Ec2 -> Network & security -> Key Pairs section
2. Go to cloud formation -> create stack and ** upload jira-stack.yml ** template file and start it.


Once complete get to your EC2 instance and check hostname and check if jira service is running by firing below command

*systemctl status jira.service*
